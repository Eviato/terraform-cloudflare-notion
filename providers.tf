provider "cloudflare" {
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

provider "random" {
  # Configuration options
}
