data "cloudflare_zones" "this" {
  filter {
    name        = split(".", var.subdomain)[1]
    lookup_type = "contains"
    match       = ".${split(".", var.subdomain)[2]}$"
    status      = "active"
  }
}

resource "cloudflare_record" "this" {
  zone_id = lookup(data.cloudflare_zones.this.zones[0], "id")
  name    = split(".", var.subdomain)[0]
  value   = "1.1.1.1"
  type    = "A"
  ttl     = 3600
}

resource "random_id" "this" {
  prefix      = "${split(".", var.subdomain)[0]}-"
  byte_length = 8
}

resource "cloudflare_worker_script" "this" {
  name = random_id.this.dec
  content = templatefile("${path.module}/notion_script.js", {
    "TF_DOMAIN"      = var.subdomain,
    "TF_NOTION_SLUG" = trimprefix(var.notion_link, "https://www.notion.so/")
  })
}

resource "cloudflare_worker_route" "this" {
  zone_id     = lookup(data.cloudflare_zones.this.zones[0], "id")
  pattern     = "${var.subdomain}/*"
  script_name = cloudflare_worker_script.this.name
}
