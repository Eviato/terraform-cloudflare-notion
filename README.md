# terraform-cloudflare-notion

Use this module to use a public notion page with a custom subdomain.

## Table of content
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Prerequisite](#prerequisite)
- [Requirements](#requirements)
- [Providers](#providers)
- [Modules](#modules)
- [Resources](#resources)
- [Inputs](#inputs)
- [Outputs](#outputs)
- [Authors](#authors)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Prerequisite

In order to make this module works you have some requirements to check :
- Have a Cloudflare account
- Have a configured root domain on Cloudflare
- Enabled Workers free plan on your Cloudflare dashboard
- Make your notion page public with `search engine indexing` enabled

Then wrote the vars file
```sh
$ vim vars.tfvars

# =======
cloudflare_email   = "example@foo.bar"
cloudflare_api_key = ""
subdomain          = "example.foo.bar"
notion_link        = "https://www.notion.so/xxxxxxx"
# Retrieve the account id on the worker main page
cloudflare_account_id = ""
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 0.14.0 |
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | ~> 2.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | 3.1.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | ~> 2.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.1.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [cloudflare_record.this](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [cloudflare_worker_route.this](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/worker_route) | resource |
| [cloudflare_worker_script.this](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/worker_script) | resource |
| [random_id.this](https://registry.terraform.io/providers/hashicorp/random/3.1.0/docs/resources/id) | resource |
| [cloudflare_zones.this](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/data-sources/zones) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cloudflare_account_id"></a> [cloudflare\_account\_id](#input\_cloudflare\_account\_id) | Account id, available in worker page. | `string` | n/a | yes |
| <a name="input_cloudflare_api_key"></a> [cloudflare\_api\_key](#input\_cloudflare\_api\_key) | The Cloudflare API key. This can also be specified with the CLOUDFLARE\_API\_KEY shell environment variable. | `string` | n/a | yes |
| <a name="input_cloudflare_email"></a> [cloudflare\_email](#input\_cloudflare\_email) | The email associated with the account. This can also be specified with the CLOUDFLARE\_EMAIL shell environment variable. | `string` | n/a | yes |
| <a name="input_notion_link"></a> [notion\_link](#input\_notion\_link) | The notion page link. | `string` | n/a | yes |
| <a name="input_subdomain"></a> [subdomain](#input\_subdomain) | Subdomain name for the notion page. | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Authors

- BRUNET Maxime <mbrunet@dawtio.cloud>
- Special thanks to [fruition website](https://fruitionsite.com/) for the idea.
