variable "cloudflare_email" {
  type        = string
  description = "The email associated with the account. This can also be specified with the CLOUDFLARE_EMAIL shell environment variable."
}

variable "cloudflare_api_key" {
  type        = string
  description = "The Cloudflare API key. This can also be specified with the CLOUDFLARE_API_KEY shell environment variable."
}

variable "cloudflare_account_id" {
  type        = string
  description = "Account id, available in worker page."
}

variable "subdomain" {
  type        = string
  description = "Subdomain name for the notion page."
}

variable "notion_link" {
  type        = string
  description = "The notion page link."
}
